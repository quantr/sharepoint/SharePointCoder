'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import { SharePointNodeProvider } from './SharePointNodeProvider';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "sharepoint-coder" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('extension.sayHello', () => {
		// The code you place here will be executed every time your command is executed

		// Display a message box to the user
		vscode.window.showInformationMessage('Hello World!');

		let settingsUri = 'peter://my-extension/fake/path/to/settings';

		// open a fake document (content served from the SettingsProvider)
		vscode.workspace.openTextDocument(vscode.Uri.parse(settingsUri))
			.then(doc => vscode.window.showTextDocument(doc)
				.then(editor => {
					console.log(editor);
				}));
	});

	context.subscriptions.push(disposable);


	// webview
	context.subscriptions.push(vscode.commands.registerCommand('catCoding.start', () => {
		const panel = vscode.window.createWebviewPanel('catCoding', "Cat Coding", vscode.ViewColumn.One, {});

		let iteration = 0;
		const updateWebview = () => {
			const cat = iteration++ % 2 ? 'Compiling Cat' : 'Coding Cat'
			panel.title = cat;
			panel.webview.html = getWebviewContent(cat);
		}

		// Set initial content
		updateWebview();

		// And schedule updates to the content every second
		const interval = setInterval(updateWebview, 1000);

		panel.onDidDispose(() => {
			// When the panel is closed, cancel any future updates to the webview content
			clearInterval(interval);
		}, null, context.subscriptions);
	}));
	// end webview

	// content provider
	const provider = new CustomContentProvider();
	const providerRegistrations = vscode.Disposable.from(
		vscode.workspace.registerTextDocumentContentProvider('peter', provider)
	);

	context.subscriptions.push(
		providerRegistrations
	);

	vscode.workspace.onDidChangeTextDocument((e: vscode.TextDocumentChangeEvent) => {
		console.log("fuck=" + e.document.getText());
		// if (e.document === vscode.window.activeTextEditor.document) {
		provider.update(vscode.Uri.parse('peter://my-extension/fake/path/to/settings'));
		// }
	});
	// end content provider

	// tree
	// const rootPath = vscode.workspace.rootPath;
	const sharepointNodeProvider = new SharePointNodeProvider();
	vscode.window.registerTreeDataProvider('sharepoint-server', sharepointNodeProvider);
	// end tree


}

class CustomContentProvider implements vscode.TextDocumentContentProvider {
	private _onDidChange = new vscode.EventEmitter<vscode.Uri>();

	public update(uri: vscode.Uri) {
		this._onDidChange.fire(uri);
	}

	get onDidChange(): vscode.Event<vscode.Uri> {
		return this._onDidChange.event;
	}

	provideTextDocumentContent(uri: vscode.Uri): string | Thenable<string> {
		let editor = vscode.window.activeTextEditor;
		return "fuck " + editor!.document.getText();
	}
}

const cats = {
	'Coding Cat': 'https://media.giphy.com/media/JIX9t2j0ZTN9S/giphy.gif',
	'Compiling Cat': 'https://media.giphy.com/media/mlvseq9yvZhba/giphy.gif'
};

function getWebviewContent(cat: keyof typeof cats) {
	return `<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>Cat Coding</title>
			<style>
			body{
				color: green;
			}
			</style>
		</head>
		<body>
			<img src="${cats[cat]}" width="300" />
		</body>
		</html>`;
}

// this method is called when your extension is deactivated
export function deactivate() {
}