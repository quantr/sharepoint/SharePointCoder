import * as vscode from 'vscode';
import * as path from 'path';

export class SharePointNodeProvider implements vscode.TreeDataProvider<Dependency> {

	private _onDidChangeTreeData: vscode.EventEmitter<Dependency | undefined> = new vscode.EventEmitter<Dependency | undefined>();
	readonly onDidChangeTreeData: vscode.Event<Dependency | undefined> = this._onDidChangeTreeData.event;

	// constructor(private workspaceRoot: string) {
	// }

	// refresh(): void {
	// 	this._onDidChangeTreeData.fire();
	// }

	getTreeItem(element: Dependency): vscode.TreeItem {
		// return element;
		return element;
	}

	getChildren(element?: Dependency): Thenable<Dependency[]> {
		return new Promise(resolve => {
			let d1: Dependency = new Dependency("d1");
			let d2: Dependency = new Dependency("d2");
			let children: Dependency[] = Array(d1, d2);
			resolve(children);
		});
	}

}

class Dependency extends vscode.TreeItem {

	constructor(
		public readonly label: string,
	) {
		super(label);
	}

	iconPath = {
		light: path.join(__filename, '..', '..', 'resources', 'sharepoint.svg'),
		dark: path.join(__filename, '..', '..', 'resources', 'sharepoint.svg')
	};
}